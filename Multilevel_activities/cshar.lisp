
;;; ***************************************************************************
;;; Knowledge of human actions recognitins for Scone, used in cshar project.
;;;
;;; Author & Maintainer: Maria Campos
;;; ***************************************************************************
;;; Development since November 2014 has been supported in part by the

;;;; ARCHITECTURE EVENTS

;;; Service architecture-events.

(load-kb "cshar-components/ma-service_architecture_events")

;;;; ----------------------------------------------------------------------
;;;; EVENT CALCULUS

;;; Knowledge of event calculus.

(load-kb "cshar-components/ma-event_calculus")

;;;; ----------------------------------------------------------------------
;;;; MOVE

;;; Knowledge of actions moves.

(load-kb "cshar-components/ma-move")

;;;; ----------------------------------------------------------------------
;;;; ACTIONS

;;; Knowledge of persons actions.

(load-kb "cshar-components/ma-actions")

;;;; ----------------------------------------------------------------------
;;;; ACTIONS 1

;;; Knowledge of persons actions 1.

(load-kb "cshar-components/ma-actions_1")

;;;; ----------------------------------------------------------------------
;;;; EXPECTATIONS

;;; Knowledge of expectations

(load-kb "cshar-components/ma-expectations")

;;;; ----------------------------------------------------------------------
;;;; CONTEXT DESCRIPTION

;;; Knowledge of specific domain.

(load-kb "cshar-components/ma-context_description")

;;;; ----------------------------------------------------------------------
