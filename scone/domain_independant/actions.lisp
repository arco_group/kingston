;;;*******************************************************
;;;** --------------------------------------------------**
;;;**  RELEVANT ACTIONS INVOLVED IN THE ANALYZED VIDEOS **
;;;** This file defines and describes some of the       **
;;;** actions that might take place in the context of a **
;;;** room. 					        **
;;;** REQUIRES: move.lisp			        **
;;;** --------------------------------------------------**
;;;*******************************************************


(in-context {general})
(in-namespace "events" :include "common")
;;; This file necessarily contains some forward references.
(setq *defer-unknown-connections* t)

;;; Some roles required for the following events and actions
(new-type-role {name} {person} {thing})
(new-type-role {personLocation} {person} {place})
(new-type-role {objectLocation} {thing} {place})
(new-type {animated object} {thing})
(new-is-a {person} {animated object})
(new-type-role {body part} {person} {thing})
(new-indv-role {arm} {person} {body part} :n 2)
(new-indv-role {leg} {person} {body part} :n 2)


;; RELATIONS APPLIED TO ACTIONS
(new-relation {has point of interest in} 
	      :a-inst-of {event}
	      :b-inst-of {body part})

(new-relation {is located at}
	      :a-inst-of {thing}
	      :b-inst-of {location})

(new-relation {has position}
	      :a-inst-of {thing}
	      :b-inst-of {position})

(new-relation {is opposite action of}
	      :a-inst-of {event}
	      :b-inst-of {event}
	      :symmetric t) 
(new-relation {performs the action of}
	       :a-inst-of {animated object}
	       :b-inst-of {event})
(new-relation {is performed by}
	       :a-inst-of {event}
	       :b-inst-of {animated object})

;; DEFINITIONS DESCRIBING THE ACTION OF CHECKING THE TIME IN A WATCH (ACTION 1 FROM IXMAS)
(new-relation {is aware of}
	      :a-inst-of {person}
	      :b-inst-of {thing})

(new-event-type {check} '({event})
  :roles
  ((:type {checker} {thing})
   (:type {checked object} {thing}))
  :before
  ((new-not-statement {checker} {is aware of} {checked object}))
  :after
  ((new-statement {checker} {is aware of} {checked object})))

		
(new-event-type {check watch}
		 '({check} {action})
 :roles
 ((:indv {checking person} {checker})
  (:indv {checked time} {checked object}))
 :throughout
 ((new-is-a {checking person} {person})
  (new-is-a {checked time} {time}))
 :before      
 ((new-not-statement {checking person} {is aware of}
	{checked time}))
 :after
((new-statement {checking person} {is aware of}
	{checked time})))

(new-statement {check watch} {has point of interest in} {arm}) 

;; DEFINITIONS DESCRIBING THE ACTION OF CROSSING ARMS (ACTION 2 FROM IXMAS)
(new-relation {is in direct contact to}
	      :a-inst-of {thing}
	      :b-inst-of {thing})


(new-relation {approaches}
	      :a-inst-of {thing}
	      :b-inst-of {thing})

(new-relation  {is closed to}
	      :a-inst-of {thing}
	      :b-inst-of {thing}
	      :symmetric t)
 
(new-relation {has crossed}
	      :a-inst-of {thing}
	      :b-inst-of {thing})

(new-event-type {crossing} '({event})  
  :roles
  ((:type {crosser} {animated thing})))

(new-event-type {cross arms}
		'({crossing} {action})
 :roles
 ((:indv {left arm} {arm})
  (:indv {right arm} {arm})
  (:indv {arms to cross} {arm})
  (:rename {crosser} {person crossing arms}))
  :throughout
 ((new-is-a {person crossing arms} {person}))
 :before
 ((new-not-statement {person crossing arms}  {has crossed}  {arms to cross})
  (new-statement {left arm} {approaches} {right arm})
  (new-statement {right arm} {approaches} {left arm})
  (new-statement {left arm} {is closed to} {right arm}))
 :after
 ((new-statement {person crossing arms}  {has crossed}  {arms to cross})
  (new-statement {left arm} {is in direct contact to} {right arm})))

(new-statement {cross arms} {has point of interest in} {arm}) 


;; DEFINITIONS DESCRIBING THE ACTION OF SCRATCHING HEAD (ACTION 3 FROM IXMAS)

(new-type-role {head} {person} {body part} :n 1)
(new-type-role {hand} {person} {body part} :n 2)

(new-relation {has scratch}
	      :a-inst-of {person}
	      :b-inst-of {thing})

(new-event-type {scratch} '({event})  
  :roles
  ((:type {scratcher} {animated thing})
   (:type {scratched thing} {thing})))


(new-event-type {scratch head}
		'({scratch} {action})		
  :roles
  ((:rename {scratched thing} {scratched head})
   (:rename {scratcher} {scratcher hand}))
  :throughout
  ((new-is-a {scratcher hand} {hand}))
  :before
  ((new-statement {scratcher hand} {approaches} {scratched head})
   (new-not-statement {scratcher hand} {is in direct contact to}
		{scratched head}))
  :after
  ((new-statement {scratcher hand} {is in direct contact to}
		{scratched head})))

(new-statement {scratch head} {has point of interest in} {arm}) 
(new-statement {scratch head} {has point of interest in} {head}) 

;; DEFINITIONS DESCRIBING THE ACTION OF SITTING DOWN (ACTION 4 FROM IXMAS)


(new-type-role {feet} {person} {body part} :n 1)

(new-relation {rests upon}
	      :a-inst-of {thing}
	      :b-inst-of {thing})


(new-relation {stands upon}
	      :a-inst-of {person}
	      :b-inst-of {body part})

(new-event-type {sit} '({event})  
  :roles
  ((:type {sitter} {animated thing})
   (:type {sitting surface} {surface})))

(new-event-type {sit down}
		'({sit} {action})		
  :roles
  ((:rename {sitter} {person sitting}))
  :throughout
  ((new-is-a {person sitting} {person}))
  :before
  ((new-not-statement {person sitting} {rests upon} {sitting surface})
   (new-statement {person sitting} {stands upon} {feet})
   (new-not-statement {person sitting} {is in direct contact to} {sitting surface}))
  :after
 ((new-statement {person sitting} {rests upon} {sitting surface})
   (new-not-statement {person sitting} {stands upon} {feet})
  (new-statement {person sitting} {is in direct contact to} {sitting surface})))

(new-statement {sit down} {has point of interest in} {leg}) 



;; DEFINITIONS DESCRIBING THE ACTION OF GETTING UP (ACTION 5 FROM IXMAS)

;(new-event-type {get up} '({event})  
;  :roles
;  ((:type {stander} {animated thing})
;   (:type {resting surface} {surface})))


(new-event-type {get up}
		'({action})		
  :roles
  ((:type {person standing up} {animated thing})
   (:type {resting surface} {surface}))
  :throughout
  ((new-is-a {person standing up} {person}))
   :before
  ((new-statement {person standing up} {rests upon} {resting surface})
   (new-statement {person standing up} {stands upon} {resting surface})
   (new-statement {person standing up} {is in direct contact to} {resting surface}))
  :after
  ((new-not-statement {person standing up} {rests upon} {resting surface})
   (new-not-statement {person standing up} {stands upon} {feet})
   (new-not-statement {person standing up} {is in direct contact to} {resting surface})))

(new-statement {get up} {is opposite action of} {sit down})
(new-statement {get up} {has point of interest in} {leg}) 


;; DEFINITIONS DESCRIBING THE ACTION OF SITTING DOWN (ACTION 7 FROM IXMAS)

(new-event-type {walk towards}
		 '({walk} {move} {action}) 
 :roles
 ((:rename {agent} {person walking}))
 :throughout
 ((new-is-a {person walking} {person})
  (new-not-eq {origin} {destination}))
 :before      
 ((new-not-statement {person walking} {is located at}
	{destination}))
 :after
 ((new-statement{person walking} {is located at}
	{destination})))

(new-statement {walk towards} {has point of interest in} {leg}) 


;;FLUENTS NEED TO BE ADDED IN ORDER TO DESCRIBE THE WALKING ACTION


;; DEFINITIONS DESCRIBING THE ACTION OF TURNING AROUND (ACTION 6 FROM IXMAS)

(new-event-type {turn around}
		'({walk} {walk towards} {action})		
  :roles
  ((:rename {agent} {person turning}))
  :throughout
  ((new-is-a {person turning} {person})
   (new-eq {origin} {destination})
   (new-statement {person turning} {performs the action of} {walk towards}))
  :before
   ((new-statement {person turning} {is located at} {origin})
    (new-statement {person turning} {is located at} {destination}))
  :after
  ((new-statement {person turning} {is located at} {origin})
   (new-statement {person turning} {is located at} {destination})))

(new-statement {turn around} {has point of interest in} {leg}) 

;; DEFINITIONS DESCRIBING THE ACTION OF WAVING (ACTION 8 FROM IXMAS)
(new-type-role {torso} {person} {body part} :n 1)

;(new-event-type {wave} '({event})  
;  :roles
;  ((:type {waving element} {animated object})
;   (:type {waved object} {thing})))

(new-event-type {wave}
		'({action})		
 :roles
  ((:type {waving person} {animated object})
   (:type {waving hand} {animated object}))
 :throughout
 ((new-is-a {waving hand} {hand})
  (new-statement {waving hand} {performs the action of} {move}))
 :after
 ((new-not-statement {waving hand} {is in direct contact to} {torso})))
(new-statement {wave} {has point of interest in} {arm}) 

;; DEFINITIONS DESCRIBING THE ACTION OF PUNCHING (ACTION 9 FROM IXMAS)
(new-type-role {fist} {person} {body part} :n 2)
(new-relation {has hit}
	      :a-inst-of {thing}
	      :b-inst-of {thing})

(new-event-type {punching} '({event})  
  :roles
  ((:type {punching element} {animated object})
   (:type {punched object} {thing})))

(new-event-type {punch}
		'({punching} {action})		
 :roles
  ((:rename {agent} {punching person})
   (:rename {punching element} {punching fist}))
 :throughout
 ((new-is-a  {punching fist} {fist})
  (new-is-a {punching person} {person}))
 :before
 ((new-not-statement {punching fist} {is in direct contact to} {punched object})
  (new-statement {punching person} {approaches} {punched object})
  (new-statement {punching person} {is closed to} {punched object}))
 :after
 ((new-statement {punching person} {has hit} {punched object})))
(new-statement {punch} {has point of interest in} {arm}) 

;; DEFINITIONS DESCRIBING THE ACTION OF KICKING (ACTION 10 FROM IXMAS)
(new-event-type {kicking} '({event})  
  :roles
  ((:type {kicking element} {animated object})
   (:type {kicked object} {thing})))

(new-event-type {kick}
		'({kicking} {action})		
 :roles
  ((:rename {agent} {kicking person})
   (:rename {kicking element} {kicking foot})
   (:indv {non-kicking foot} {foot})
   (:indv {walking floor} {surface}))
 :throughout
 ((new-is-a  {kicking foot} {foot})
  (new-is-a {kicking person} {person})
  (new-statement {non-kicking foot} {rests upon} {walking floor}))
 :before
 ((new-not-statement {kicking foot} {is in direct contact to} {kicked object})
  (new-statement {kicking person} {approaches} {kicked object})
  (new-statement {kicking person} {is closed to} {kicked object}))
 :after
 ((new-statement {kicking person} {has hit} {kicked object})))

(new-statement {kick} {has point of interest in} {leg}) 


;; DEFINITIONS DESCRIBING THE ACTION OF POINTING (ACTION 11 FROM IXMAS)
(new-type-role {hand finger} {person} {body part} :n 10)

(new-relation {focuses attention on}
	      :a-inst-of {person}
	      :b-inst-of {thing})

(new-relation {points at}
	      :a-inst-of {person}
	      :b-inst-of {location})

(new-event-type {point} '({event})  
  :roles
  ((:type {pointing element} {animated object})
   (:type {pointed object} {thing})))

(new-event-type {point at}
		'({point} {action})		
 :roles
  ((:rename {agent} {pointing person})
   (:rename {pointing element} {pointing finger})
   (:indv {pointed object location} {location}))
 :throughout
 ((new-is-a  {pointing finger} {hand finger})
  (new-is-a {pointing person} {person})
  (new-not-statement {pointing finger} {is in direct contact to} {torso}))
 :before
 ((new-not-statement {pointing person} {focuses attention on} {pointed object})
  (new-not-statement {pointing finger} {points at} {pointed object location}))
 :after
 ((new-not-statement {pointing person} {focuses attention on} {pointed object})
  (new-not-statement {pointing finger} {points at} {pointed object location})))

(new-statement {point at} {has point of interest in} {arm}) 


;; DEFINITIONS DESCRIBING THE ACTION OF PICKING UP (ACTION 12 FROM IXMAS)

(new-relation {is hold by}
	      :a-inst-of {thing}
	      :b-inst-of {person})

(new-event-type {pick} '({event})  
  :roles
  ((:type {picking element} {animated object})
   (:type {picked object} {thing})
   (:indv {picked object origin} {location})
   (:indv {picked object end location} {location}))
  :throughout
  ((new-is-a {picking element} {hand})
   (new-not-eq {picked object origin} {picked object end location})))

(new-event-type {pick up}
		'({pick} {action})		
 :roles
  ((:rename {agent} {picking person})
   (:rename {picking element} {picking hand}))
 :throughout
 ((new-indv-role {picking person location}  {picking person} {location})
  (new-indv-role {picked object location}  {picked object} {location}))
 :before
 ((new-not-eq {picking person location} {picked object location})
  (the-x-of-y-is-z {picked object location} {picked object} {picked object origin})
  (new-statement {picking person} {approaches} {picked object location})
  (new-not-statement {picked object} {is hold by} {picking hand}))
 :after
 ((new-eq {picking person location} {picked object location})
  (new-statement {picking person} {is in direct contact to} {picked object})
  (the-x-of-y-is-z {picked object location} {picked object} {picked object end location})
  (new-eq {picked object end location} {picking person location})
  (the-x-of-y-is-z {picking person location} {picking person} {picked object location})
  (new-statement {picked object} {is hold by} {picking hand})))

(new-statement {pick up} {has point of interest in} {arm}) 

(new-event-type {lower down}
		'({event} {action})
 :roles
 ((:rename {agent} {person lowering down})
  (:indv {person lowering down feet} {feet})
  (:indv {person lowering down legs} {leg}))
 :throughout
 ((the-x-of-y-is-z {feet} {person lowering down} {person lowering down feet})
  (the-x-of-y-is-z {leg} {person lowering down} {person lowering down legs}))
 :before
 ((new-statement {person lowering down} {stands upon} {person lowering down feet}))
 :after
 ((new-statement {person lowering down} {stands upon} {person lowering down legs})))


;; DEFINITIONS DESCRIBING THE ACTION OF THROWING OVER HEAD (ACTION 13 FROM IXMAS)
(new-type-role {both hands} {person} {body part} :n 1)
(new-type-role {trajectory} {thing} {line})

(new-relation {stands over}
	      :a-inst-of {thing}
	      :b-inst-of {thing})

(new-event-type {throw} '({event})  
  :roles
  ((:type {throwing element} {animated object})
   (:type {thrown object} {thing})
   (:indv {thrown object origin} {location})
   (:indv {thrown object end location} {location}))
  :throughout
  ((new-is-a {throwing element} {both hands})
   (new-not-eq {thrown object origin} {thrown object end location})))

(new-event-type {person throwing}
		'({throw} {action})
 :roles
  ((:rename {agent} {throwing person})
   (:rename {throwing element} {throwing hands}))
 :throughout
 ((new-is-a {throwing person} {person})
  (new-indv-role {throwing person location}  {throwing person} {location})
  (new-indv-role {thrown object location}  {thrown object} {location})
  (new-indv-role {thrown object trajectory} {thrown object} {trajectory}))
 :before
 ((new-eq {throwing person location} {thrown object location})
  (the-x-of-y-is-z {thrown object location} {thrown object} {thrown object origin})
  (new-statement {throwing person} {is in direct contact to} {thrown object location})
  (new-statement {thrown object} {is hold by} {throwing hands}))
 ;; since knowing the trajectory of the thrown object is not relevant
 ;; for what we are doing at the moment, we are skipping the fluent
 ;; description for the object trajectory
 :after
 ((new-not-eq {throwing person location} {thrown object location})
  (the-x-of-y-is-z {thrown object location} {thrown object} {thrown object end location})
  (new-not-statement {throwing person} {is in direct contact to} {thrown object location})
  (new-not-statement {thrown object} {is hold by} {throwing hands})))


(new-event-type {throw over head}
		'({throw} {action})		
 :throughout
 ((new-indv-role {throwing person head} {throwing person} {head}))
 :before
 ((new-statement {throwing hands} {stands over} {throwing person head})
  (new-statement {thrown object} {rests upon} {throwing hands})
  (new-statement {thrown object} {is in direct contact to} {throwing hands})))
;; :after
;; ((the-x-of-y-is-z {thrown object trajectory} {straight-down)}))
;; The after context should describe the trajectory for an object
;; being thrown over the head. So, in the after context the end
;; location of the thrown object should be known. 

(new-statement {throw over head} {has point of interest in} {arm}) 

(new-event-type {throw bottom up}
		'({throw} {action})	
 :throughout
 ((new-indv-role {throwing person torso} {throwing person} {torso}))
 :before
 ((new-statement {thrown object} {is in direct contact to} {floor})
  (new-statement {thrown object} {rests upon} {floor})
  (new-statement {throwing hands} {is in direct contact to} {throwing person torso})))
(new-statement {throw bottom up} {has point of interest in} {arm})
